<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Sender;
use Illuminate\Http\Request;

class SenderController extends Controller
{

    public function index()
    {
        $notifications =  Sender::get();
        $count = count($notifications);
//        dd($count);
        $is_set = 0;
        return view('counter', compact('notifications','count', 'is_set'));
    }

    public function create()
    {
        return view('welcome');
    }

    public function store(Request $request)
    {

        $data_input = $request->all();
//        dd($data_input);
        $input = Sender::create($data_input);
//        dd($input);
        if($input instanceof Sender){
//            dd($data_input);
            event(new MyEvent($data_input['text']));
            return redirect()->route('welcome');
        }
        return 'error';
    }
}
