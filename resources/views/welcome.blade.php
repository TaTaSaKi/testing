
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>News Talk</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <style>
    .container {
      padding-top: 100px;
    }
  </style>

  <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode([
      'csrfToken' => csrf_token(),
    ]) !!};
  </script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
  <div id="app">
    <!-- home Vue component -->
    {{--<home></home>--}}
   <form method="POST" action="{{ route('store')}}">
  @csrf

    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

          <div class="form-group">
            <label for="text">Post Title</label>
            <input name="text" id="text" type="text" class="form-control">
          </div>
          {{--<div class="form-group">--}}
            {{--<label for="description">Post Description</label>--}}
            {{--<textarea name="description" id="description" rows="8" class="form-control"></textarea>--}}
          {{--</div>--}}
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>
    </div>
   </form>
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  {{-- <script>
    export default {
      data() {
        return {
          title: "", 
          description: "" 
        }
      },
      created() {
        this.listenForChanges();
      },
      methods: {
        addPost(postName, postDesc) {
          // check if entries are not empty
          if(!postName || !postDesc)
            return;
  
          // make API to save post
          axios.post('/post', {
            title: postName, description: postDesc
          }).then( response => {
            if(response.data) { 
              this.newPostTitle = this.newPostDesc = "";
            }
          })
        },
        listenForChanges() {
          Echo.channel('posts')
            .listen('PostPublished', post => {
              if (! ('Notification' in window)) {
                alert('Web Notification is not supported');
                return;
              }
  
              Notification.requestPermission( permission => {
                let notification = new Notification('New post alert!', {
                  body: post.title, // content for the alert
                  icon: "https://pusher.com/static_logos/320x320.png" // optional image url
                });
  
                // link to page on clicking the notification
                notification.onclick = () => {
                  window.open(window.location.href);
                };
              });
            })
          }
        } 
      }
  </script> --}}
</body>
</html>