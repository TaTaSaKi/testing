<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Admin | @yield('title')</title>
    @include('partials.css')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    {{--  NavBar  --}}
    @include('partials.navbar')
    {{--  SideBar  --}}
    {{--@include('partials.sidebar')--}}
    <div class="content-wrapper pt-2">
        @yield('content')
        {{--@include('partials.footer')--}}
    </div>
</div>
@include('partials.script')
@yield('script')
</body>
</html>
