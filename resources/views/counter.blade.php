<!DOCTYPE html>
<html>
<head>
  <title>Pusher Test</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('node_modules/@fortawesome/fontawesome-free/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/@fortawesome/fontawesome-free/css/brands.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/@fortawesome/fontawesome-free/css/solid.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a0ad484608743e290cdd', {
      cluster: 'ap1',
      forceTLS: true
    });


    var channel = pusher.subscribe('my-channel');
    channel.bind('MyEvent', function(data) {
        $output = JSON.stringify(data.text);
        alert(JSON.parse($output));

    });
  </script>
</head>
<body>
<div class="container-md">
    <h1>Notification</h1>
    <a href='#' class="btn btn-sm btn-primary">
      <i class="fas fa-bell">
            @if($is_set==1)
                {{$count=+1}}
            @else
                {{$count}}
            @endif
      </i>
    </a>
</div>
</body>
</html>