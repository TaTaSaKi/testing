{{-- <form action="/sender" method="POST">
    <input type="text" name="text">
    <input type="submit">
    {{ csrf_field() }}
</form> --}}
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>News Talk</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .container {
        padding-top: 100px;
        }
    </style>
    <script>
        window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                <div class="form-group">
                    <label for="text">Post Title</label>
                    <input name="text" id="text" type="text" class="form-control">
                </div>
                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
            </div>
        </div>
</body>
</html>